/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author aluno.redes
 */
public class Exceptions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //object reader

        try {
            Scanner sc = new Scanner(System.in);
            
            System.out.println("getting a file!");

            System.out.println("digit first number: ");
            int A = sc.nextInt();

            System.out.println("digit second number: ");
            int B = sc.nextInt();

            int arra[] = new int[5];

            //logic error
            /*for (int i = 0; i <= 5; i++) {//logic error
            arra[i] = i;
            System.out.println("kkk");
        }*/
            for (int i = 20; i < 10; i++) {// if i begins with with 20 and the limit is 10, there´s a logic error
                System.out.println("repeat!");
            }

            Person vet[] = new Person[5];

            Person obj = new Person();
            obj.setName("dimi");

            vet[0] = obj;

            /*for (int i = 0; i < arr.length; i++) {
            arr[i] =  
                    
            arr[i] = i+1;        
        }*/
            System.out.println("arr[i].Name = " + vet[1].getName());

            System.out.println("the sum is: " + A + B);
        }catch(InputMismatchException didi){//didi is an object of InputMismatchException      //InputMismatchException: if i digit a letter or something else like that
            System.out.println("wrong numbers, please try again with integer numbers.");        
            System.out.println(didi.getLocalizedMessage());
            System.out.println(Arrays.toString(didi.getStackTrace()));
            
        }catch(NullPointerException dede){
                System.out.println("there is a null pointer exception");
                System.out.println(dede.toString());
                
        } catch (Exception e) {//a genneric option
            System.out.println("sorry there is a Exception(Error), Try again");
        
        } finally {// finally é opcional(ele obriga a ser mostrado ou talvez executado o que está dentro dele)
            System.out.println("releasing resource: ");
            
            System.out.println("bye");
        }//end finally

        
    }

}
