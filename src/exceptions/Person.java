package exceptions;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aluno.redes
 */
public class Person {
    //class attributes
    private String name;
    private int age;
    private double height;
    private double weight;
    
    //constructor
    public Person(){
        //System.out.println("person constructor");        
    }
    
    //class methods
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age >= 0) {
            this.age = age;
        } else {
            System.out.println("age must be greater than zero");
        }
    }
    
    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        if (height >= 0.15) {
            this.height = height;
        } else {
            System.out.println("height must be equal or greater than than 0.15 meters");
        }
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        if (weight >= 1) {
            this.weight = weight;
        } else {
            System.out.println("weight must be greater than 1 kilogram!");
        }
    }

    //todo getInfo - Method to retreive all class attributes
    public String getInfo(){
        return "name: "+this.name+"\nage: "+this.age+"\nweight: "+this.weight+"\nheight: "+this.height;
    }
    
    public double BMI(){
        if ((weight >= 1) && (height >= 0.15)) {
            //valid values for calculate
            return weight / (height * height);
        } else {
            //can´t calculate BMI
            return 0;
        } 
    }
    
    /*@Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + '}';
    }*/

    
    
    
}//end class Person
